﻿using NUnit.Framework;
using System;
using BowlingGame;
namespace BowlingTest
{
    [TestFixture()]
    public class Test
    {
        Game game;
        [SetUp]
        public void CreateGame()
        {
            game = new Game();
        }
        public void ManyRoll(int rolls, int pins)
        {
            for (int i = 0; i < rolls; i++)
            {
                game.Roll(pins);
            }
        }
        [Test()]
        public void GutterGame()
        {

            ManyRoll(20, 0);
            Assert.That(game.Score(), Is.EqualTo(0));
        }
        [Test()]
        public void OnePingame()
        {

            ManyRoll(20, 1);
            Assert.That(game.Score(), Is.EqualTo(20));
        }
        [Test()]
        public void FirstSpareRestOne()
        {
            game.Roll(9);
            game.Roll(1);
            ManyRoll(18, 1);
            Assert.That(game.Score(), Is.EqualTo(29));

        }
        [Test()]
        public void FirstStrikeRestOne()
        {
            game.Roll(10);
            ManyRoll(18, 1);
            Assert.That(game.Score(), Is.EqualTo(30));
        }
        [Test()]
        public void PerfectGame()
        {
            ManyRoll(12, 10);
            Assert.That(game.Score(), Is.EqualTo(300));
        }
        [Test]
        public void LastSpare()
        {
            ManyRoll(18, 1);
            game.Roll(9);
                game.Roll(1);
            game.Roll(5);
            Assert.That(game.Score(), Is.EqualTo(33));

        }
        [Test]
        public void LastStrike()
        {
            ManyRoll(18, 1);
            game.Roll(10);
            game.Roll(1);
            game.Roll(5);
            Assert.That(game.Score(), Is.EqualTo(34));


        }


    }
}
